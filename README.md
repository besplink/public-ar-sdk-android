# Splink Android SDK
## Android SDK for the Splink AR Experiences.
*CompileSdkVersion 33, buildToolsVersion '33.0.1', Gradle 7.3.1*

### Table of contents

1. [Getting Started](#markdown-header-getting-started)
2. [Step 1](#markdown-header-step-1-add-maven-url)
3. [Step 2](#markdown-header-step-2-add-manifest-metadata)
4. [Step 3](#markdown-header-step-3-add-implementation)
5. [Step 4](#markdown-header-step-4-start-the-experiences)
6. [Step 5](#markdown-header-step-5-proguard)
7. [Available Experiences](#markdown-header-available-experiences)

### Getting Started {#getting-started}
#### Step 1: Add maven URL {#getting-started-step-1}
Add snippet below to your root build script file:


```
buildscript { 
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:7.3.1'
    } 
}
allprojects { 
    repositories {
        google()
        mavenCentral()
        maven {
            url "https://splink.jfrog.io/artifactory/splink-android-sdk/"
            credentials {
                username "splink-sdk"
                password "splinkSDK0"
            }
        }
    } 
}
```
#### Step 2: Add Manifest metadata {#getting-started-step-2}
Add snippet below to the your app's manifest, inside the 'application' segment: 

```
<application …>
    …

    <meta-data android:name="com.google.ar.core" android:value="optional" />
</application>
```

#### Step 3: Add implementation {#getting-started-step-3}
Add snippet below to the build.gradle of your main android library: 

```
implementation 'com.splink.sdk:splinkSDK:1.7.2'
```
#### Step 4: Start the experiences {#getting-started-step-4}
The following example shows the experiences being started from a button.

**strings.xml – Define your API_KEY**  
To start the experiences, it is required an unique key that will identify your app. Open your strings.xml located at res/values/strings.xml and add your key:
 
```xml
<string name="splink_api_key">12345DEFAULT</string>
```

**XML - Adding a button to the layout**  
Start by adding a button to your activity or fragment xml:

```xml
<androidx.appcompat.widget.AppCompatButton
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="start"
    android:id="@+id/startAR"/>
```

You can latter customize this, but as an example, a button with “START” written on it will be used and assigned the id “startAR”.

**Java – Setting up the button**  
In your Activity or Fragment add:
 
```java
@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState); setContentView(R.layout.activity_main);
    Button startAR = findViewById(R.id.startAR);
    String APIKEY = getString(R.string.splink_api_key);
  startAR.setOnClickListener(view ->
                 SplinkSDK.startARExperience(APIKEY, SplinkSDK.Experience.PORTAL,languageCode, activity));
 
```
The language code should follow Android's language code, example "en", "es", "pt", "fr", etc.

#### Step 5: Proguard {#getting-started-step-5}
If your app uses proguard add the following lines to your proguard-rules.pro:


```java
-keep public class com.splink.sdk.** {*; }
-keep class com.unity3d.** {*; }
-keep class com.google.androidgamesdk.ChoreographerCallback { *; }
-keep class com.google.androidgamesdk.SwappyDisplayManager { *; }
-keep class bitter.jnibridge.* { *; }

```


### Available Experiences {#available-experiences}
The experiences currently available are:

**Portal Experience**  
Walk inside your club’s stadium and other facilities in this AR experience.
 
```java
SplinkSDK.startARExperience(APIKEY, SplinkSDK.Experience.PORTAL,languageCode, activity);
```


**Player Photo:**  
Bring your club’s players into your room.
 
```java
SplinkSDK.startARExperience(APIKEY, SplinkSDK.Experience.PLAYER_PHOTO,languageCode, activity);
```


**Player Stats:**  
Your favorite player stats in AR.
 
```java
SplinkSDK.startARExperience(APIKEY, SplinkSDK.Experience.PLAYER_STATS,languageCode, activity);
```

**Facemask:**  
Your favorite player stats in AR.
 
```java
SplinkSDK.startARExperience(APIKEY, SplinkSDK.Experience.FACEMASK,languageCode, activity);
```



**Jersey:**  
Jersey experience
 
```java
SplinkSDK.startARExperience(APIKEY, SplinkSDK.Experience.JERSEY,languageCode, activity);
```